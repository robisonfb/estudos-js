var num = document.querySelector('#num')
var valor = document.querySelector('#valor')
var res = document.querySelector('#res')

var valores = []

function enumero(num){
    if(Number(num) >= 1 && Number(num) <= 100){
        return true
    }else{
        return false
    }
}

function nalista(num, lista){
    if (lista.indexOf(Number(num)) != -1){
        return true
    }else {
        return false
    }
}

function additem(n){
    valores.push(Number(n))
    let item = document.createElement('option')
    item.text = `Valor ${n} adicionado`
    item.value = `val-${n}`
    valor.appendChild(item)
    res.innerHTML = ''
}

function adicionar(){
    if (enumero(num.value) && !nalista(num.value, valores)) {
        additem(num.value)
    } else {
        alert('Valor invalido ou já esta na lista!');
    }
    num.value = ''
    num.focus()
}

function finalizar(){
 if (valores.length == 0){
    alert('Adiciona valores antes de finalizar');
 }else{
    let total = valores.length
    let maior = valores[0]
    let menor = valores[0]
    let soma = 0
    let media = 0
    for(let pos in valores){
        soma += valores[pos]

        if(valores[pos] > maior){
            maior = valores[pos]
        }
        if (valores[pos] < menor) {
            menor = valores[pos]
        }
    }
    media = soma / total

    res.innerHTML = ''
    res.innerHTML += `<p> Temos ${total} valores adicionados`
    res.innerHTML += `<p> O Maior numero informado e ${maior}`
    res.innerHTML += `<p> O Menor numero informado e ${menor}`
    res.innerHTML += `<p> A soma dos valores e ${soma}`
    res.innerHTML += `<p> A media dos valores e ${menia}`
 }
}