function carregar(){    
    var msg = document.getElementById('msg')
    var imagem = document.getElementById('imagem') 
    var data = new Date()
    var hora = data.getHours()

    if (hora >= 0 && hora < 12){
        imagem.src = "img/manha.png"
        msg.innerHTML = `<p>Bom dia</p>`
        msg.innerHTML += `Agora são ${hora} horas.`
        document.body.style.background = "rgb(234 225 200)"

    }else if (hora >= 12 && hora <= 18){
        imagem.src = "img/tarde.png"
        msg.innerHTML = `<p>Boa Tarde</p>`
        msg.innerHTML += `Agora são ${hora} horas.`
        document.body.style.background = "rgb(118 50 19)"

    }else{
        imagem.src = "img/noite.png"
        msg.innerHTML = `<p>Boa Noite</p>`
        msg.innerHTML += `Agora são ${hora} horas.`
        document.body.style.background = "rgb(100 92 88)"

    }
}
