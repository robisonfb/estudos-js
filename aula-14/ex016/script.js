function contar(){
    var inicio = document.querySelector('#inicio')
    var fim = document.querySelector('#fim')
    var passo = document.querySelector('#passo')
    var res = document.querySelector('#res')

    if (inicio.value.length == 0 || fim.value.length == 0 || passo.value.length == 0){
        alert('[ERROR] Faltam dados');
    }else{
        res.innerHTML = `contando : <br />`
        var i = Number(inicio.value)
        var f = Number(fim.value)
        var p = Number(passo.value)
        if (p <= 0){
            alert('Passo invalido! Considerando passo 1');
            p = 1
        }
        if(i < f){
            // Contagem crescente
            for (var c = i; c <= f; c  +=p ) {
                res.innerHTML += `${c} \u{1F449}`
            }
        }else{
            // Contagem regressiva
            for (var c = i; c >= f; c -= p) {
                res.innerHTML += `${c} \u{1F449}`
            }
            
        }
        res.innerHTML += `${c} \u{1F3C1}`
    }





    
}