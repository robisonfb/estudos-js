function gerar(){
    var num = document.querySelector('#num')
    var tab = document.querySelector('#tab')
    tab.innerHTML = ''
    if(num.value.length == 0){
        alert('Por favor, digite um Número!');
    }else{
        var n = Number(num.value)
        for (var c = 1; c <= 10; c++) {
            var item = document.createElement('option')
            item.text = `${n} X ${c} = ${n*c}`
            item.value = `tab${c}`
            tab.appendChild(item)
        }
    }
}